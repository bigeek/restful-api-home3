const {
    Schema,
    model
} = require('mongoose');

const userSchema = new Schema({
    id: {
        type: Number,
        required: true
    },
    name: String,
    username: String,
    email: String,
    phone: String,
    country: String,
    age: Number,
    gender: String,
    winDate: Date
}, {
    collection: 'users',
    versionKey: false
});

const User = model('User', userSchema);

module.exports = User;