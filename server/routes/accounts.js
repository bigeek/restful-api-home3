const express = require('express');
const router = express.Router();
const users = require('../controllers/users');

router.get('/', users.userController.get);
router.get('/:id', users.userController.get);
router.put('/users/:id', users.userController.put);
router.delete('/users/:id', users.userController.delete);
router.post('/users/', users.userController.post);

module.exports = router;