const mongoose = require("mongoose");
const ck = require("ckey");

const db = ck.DATABASE;

const User = require("../mongo/User");

const options = {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true
};

exports.userController = {
  get(req, res) {
    if (req.params && req.params.id) {
      const { id } = req.params;
      console.log("single data requested");
      mongoose
        .connect(db, options)
        .then(async () => {
          const result = await User.find({ id: id });

          if (result.length) {
            res.json(result);
          } else {
            res.status(404).send('{error: "no user found"}');
          }
        })
        .catch(err => {
          console.error("db error", err);
          res.status(500).send(err.message);
        });
    } else {
      console.log("all data requested");
      mongoose
        .connect(db, options)
        .then(async () => {
          const result = await User.find({});

          if (result) res.json(result);
          res.status(404).send('{error: "no user found"}');
        })
        .catch(err => {
          console.error("db error", err);
          res.status(500).send(err.message);
        });
    }
  },
  
  post(req, res) {
    mongoose
      .connect(db, options)
      .then(async () => {
        const {
          id = null,
          name = null,
          username = null,
          email = null,
          phone = null,
          country = null,
          age = null,
          gender = null,
          winDate = null
        } = req.body;
        const user = new User({
          id,
          name,
          username,
          email,
          phone,
          country,
          age,
          gender,
          winDate
        });
        const result = await user.save();
        if (result) {
          res.json({ response: "done" });
        } else {
          res.status(404).send('{error: "no user found"}');
        }
      })
      .catch(err => {
        console.error("db error", err);
        res.status(500).send(err.message);
      });
  },

  put(req, res) {
    mongoose
      .connect(db, options)
      .then(async () => {
        const {id} = req.params;
        const body = req.body;
        const result = await User.updateOne({ id }, body);
        if (result.ok) {
			res.json({ response: "done" });
        } else {
			res.status(404).send('{error: "no user found"}');
        }
      })
      .catch(err => {
        console.error("db error", err);
        res.status(500).send(err.message);
      });
  },

  delete(req, res) {
    mongoose
      .connect(db, options)
      .then(async () => {
        const { id } = req.params;
        const result = await User.deleteOne({ id });
        if (result.deletedCount) {
          res.json({ response: "done" });
        } else {
          res.status(404).send('{error: "no user found"}');
        }
      })
      .catch(err => {
        console.error("db error", err);
        res.status(500).send(err.message);
      });
  }
};
