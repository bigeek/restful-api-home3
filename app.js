const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const accountsRouter = require('./server/routes/accounts');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser()); //

app.use('/accounts', accountsRouter);

app.all('*', (req,res) => res.send('All other requests')); // add 404 in future

module.exports = app;
