# RESTful api homework3 Ninja winners management

#### _API LOCATION_

https://documenter.getpostman.com/view/9326601/SWLYCBoW

#### _DEPLOYED BACKEND DEMO API_

http://home3.chepa.net

# About

RESTful api that gives you to add/delete/change/view winners of NINJA

# Overview

## 5 calls available:

### GET:

- GET */accounts/%id% - get info about %id% winner
- GET */accounts/ - get info about all winners

### POST:

- POST - */accounts/users - add new winner to db

### PUT:

- PUT - */accounts/users/%id% - change winner with %id%

### DELETE:

- DELETE - */accounts/users/%id - delete winner with %id%

# Error Codes

- **404** -- wrong user id
- **500** -- wrong body type